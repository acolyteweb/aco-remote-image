<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://acolyte.ws
 * @since             1.0.0
 * @package           Aco_remote
 *
 * @wordpress-plugin
 * Plugin Name:       Aco Remote Image
 * Plugin URI:        https://acolyte.ws
 * Description:       Changes the attachment URL path to a remote server
 * Version:           1.0.0
 * Author:            Jessy Diamond
 * Author URI:        https://acolyte.ws
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       aco_remote
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ACO_REMOTE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-aco_remote-activator.php
 */
function activate_aco_remote() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-aco_remote-activator.php';
	Aco_remote_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-aco_remote-deactivator.php
 */
function deactivate_aco_remote() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-aco_remote-deactivator.php';
	Aco_remote_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_aco_remote' );
register_deactivation_hook( __FILE__, 'deactivate_aco_remote' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-aco_remote.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_aco_remote() {

	$plugin = new Aco_remote();
	$plugin->run();

}
run_aco_remote();
