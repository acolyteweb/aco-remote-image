<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://acolyte.ws
 * @since      1.0.0
 *
 * @package    Aco_remote
 * @subpackage Aco_remote/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Aco_remote
 * @subpackage Aco_remote/includes
 * @author     Jessy Diamond <jessy@acolyte.ws>
 */
class Aco_remote_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
