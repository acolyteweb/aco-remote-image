<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://acolyte.ws
 * @since      1.0.0
 *
 * @package    Aco_remote
 * @subpackage Aco_remote/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Aco_remote
 * @subpackage Aco_remote/admin
 * @author     Jessy Diamond <jessy@acolyte.ws>
 */
class Aco_remote_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aco_remote_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aco_remote_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/aco_remote-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aco_remote_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aco_remote_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/aco_remote-admin.js', array( 'jquery' ), $this->version, false );

	}


    public function aco_remote_settings_add_plugin_page() {
        add_menu_page(
            'Aco Remote Image Settings', // page_title
            'Aco Remote Image Settings', // menu_title
            'manage_options', // capability
            'aco-remote-settings', // menu_slug
            array( $this, 'aco_remote_settings_create_admin_page' ), // function
            'dashicons-admin-generic', // icon_url
            81 // position
        );
    }

    public function aco_remote_settings_create_admin_page() {
        $this->aco_remote_settings_options = get_option( 'aco_remote_settings_option_name' ); ?>

        <div class="wrap">
            <h2>Aco Remote Settings</h2>
            <p></p>
            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <?php
                settings_fields( 'aco_remote_settings_option_group' );
                do_settings_sections( 'aco-remote-settings-admin' );
                submit_button();
                ?>
            </form>
        </div>
    <?php }

    public function aco_remote_settings_page_init() {
        register_setting(
            'aco_remote_settings_option_group', // option_group
            'aco_remote_settings_option_name', // option_name
            array( $this, 'aco_remote_settings_sanitize' ) // sanitize_callback
        );

        add_settings_section(
            'aco_remote_settings_setting_section', // id
            'Settings', // title
            array( $this, 'aco_remote_settings_section_info' ), // callback
            'aco-remote-settings-admin' // page
        );

        add_settings_field(
            'remote_website_url', // id
            'Remote Website URL', // title
            array( $this, 'remote_website_url_callback' ), // callback
            'aco-remote-settings-admin', // page
            'aco_remote_settings_setting_section' // section
        );
    }

    public function aco_remote_settings_sanitize($input) {
        $sanitary_values = array();
        if ( isset( $input['remote_website_url'] ) ) {
            $sanitary_values['remote_website_url'] = sanitize_text_field( $input['remote_website_url'] );
        }

        return $sanitary_values;
    }

    public function aco_remote_settings_section_info() {

    }

    public function remote_website_url_callback() {
        printf(
            '<input class="regular-text" type="text" name="aco_remote_settings_option_name[remote_website_url]" id="remote_website_url" value="%s">',
            isset( $this->aco_remote_settings_options['remote_website_url'] ) ? esc_attr( $this->aco_remote_settings_options['remote_website_url']) : ''
        );
    }

}
