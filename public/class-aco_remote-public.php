<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://acolyte.ws
 * @since      1.0.0
 *
 * @package    Aco_remote
 * @subpackage Aco_remote/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Aco_remote
 * @subpackage Aco_remote/public
 * @author     Jessy Diamond <jessy@acolyte.ws>
 */
class Aco_remote_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aco_remote_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aco_remote_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/aco_remote-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aco_remote_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aco_remote_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/aco_remote-public.js', array( 'jquery' ), $this->version, false );

	}

    public function aco_replace_image_urls() {
        function aco_wp_get_attachment_url( $url, $post_id) {
            $option = get_option('aco_remote_settings_option_name');
            $live_url = isset($option['remote_website_url']) ? $option['remote_website_url'] : null;
            $wp_url = getenv('WP_HOME');
            if ( $file = get_post_meta( $post_id, '_wp_attached_file', true) ) {
                if ( ($uploads = wp_upload_dir()) && false === $uploads['error'] ) {
                    if ( file_exists( $uploads['basedir'] .'/'. $file ) ) {
                        return $url;
                    }
                }
            }
            return str_replace( $live_url, $wp_url, $url );
        }

        function aco_wp_get_attachment_image_src( $image, $attachment_id, $size, $icon ) {
            $option = get_option('aco_remote_settings_option_name');
            $live_url = isset($option['remote_website_url']) ? $option['remote_website_url'] : null;
            $wp_url = getenv('WP_HOME');
            if ( $file = get_post_meta( $attachment_id, '_wp_attached_file', true) ) {
                if ( ($uploads = wp_upload_dir()) && false === $uploads['error'] ) {
                    if ( file_exists( $uploads['basedir'] .'/'. $file ) ) {
                        return $image;
                    }
                }
            }

            $image_src = str_replace( $wp_url, $live_url, $image[0] );
            $image[0] = $image_src;

            return $image;
        }

        $option = get_option('aco_remote_settings_option_name');
        if(isset($option['remote_website_url'])){

            if ( $option['remote_website_url'] && getenv('WP_HOME') ) {
                if ( $option['remote_website_url'] != getenv('WP_HOME') ){
                    add_filter('wp_get_attachment_url', 'aco_wp_get_attachment_url', 10, 2 );
                    add_filter('wp_get_attachment_image_src', 'aco_wp_get_attachment_image_src', 10, 4 );
                }
            }
        }
    }




}
